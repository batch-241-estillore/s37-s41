
const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");


//Route for creating a course
router.post("/addCourse", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
	courseController.addCourse(req.body).then(resultFromController => res.send(true));
	}else{
		return res.send(false)
	}
});

//Route for retrieving all the courses
router.get("/all", (req,res) => {
	courseController.getAllCourses().then(resultFromController => 
		res.send(resultFromController))
})


//Route for retrieving active courses
router.get("/active", (req,res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})


//Route for retrieving specific course
router.get("/:courseId", (req,res) =>{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});


//Route to update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


//Route to archive course
router.patch("/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});




module.exports = router;