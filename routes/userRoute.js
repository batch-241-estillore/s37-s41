const express = require("express");

const router = express.Router();
const userController = require("../controllers/userController");

const auth = require("../auth");


//Create a route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

//Route for User Registration
router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//Route for User Authentication
router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


//Route for getting User document but reassign password into ""
//auth.verify is middleware
router.post("/details", auth.verify/*this is middleware*/, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	console.log(userData)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
});


//Route to enroll user to a course
/*router.post("/enroll", (req,res) => {
	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	};
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});*/


router.post("/enroll", auth.verify, (req,res) => {


	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	};
	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;