const User = require("../models/User");
const Course = require("../models/Course");

//#include bcrypt to encrypt password
const bcrypt = require("bcrypt");
const auth = require("../auth");


//Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)

	})
	return newUser.save().then((user,error) => {
		if (error){
			return false
		} else{
			return user
		}

	})
}


module.exports.loginUser = (reqBody)=> {

	return User.findOne({email: reqBody.email}).then(result => {

			if(result == null){
				return false
			} else{
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
				if(isPasswordCorrect){
					return { access: auth.createAccessToken(result)}
				} else {
					return false
				}
			}
	})
}

//get details of one user
module.exports.getProfile = (reqBody)=> {
	//console.log(reqBody)
	//for investigation why findOne with wrong parameter works:
	return User.findOne({_id: reqBody.userId}).then(result => {

			if(result == null){
				return result = "No match"
			} 
			else{
				result.password = ""
				return result
			}

	})
}


//Enroll user to a course
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user,error) => {
			if (error){
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course,err) => {
			if(err){
				return false
			} else {
				return true
			}
		});
	});


	if(isUserUpdated && isCourseUpdated){
			return true
		} else {
			return false
		};

};
