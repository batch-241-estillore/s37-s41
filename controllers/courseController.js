const Course = require("../models/Course");
const auth = require("../auth");

module.exports.addCourse = (reqBody) => {
	//{
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
	
		return newCourse.save().then((course,error) => {
			if(error){
				return false
			}else {
				return course
			}
		})
	//}
	/*
		let message = Promise.resolve('User must be Admin to access this')
		return message.then((value) => {
			return {value};
		})
	*/
}

//Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

//Retrieve active courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

//Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

//Update specific course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	/*
		SYNTAX:
			findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error){
			return false;
		} else{
			return true;
		};
	});
};

//Archive course
module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isActive: reqBody.isActive
	};
	
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
		if(error){
			return false;
		} else{
			return true;
		};
	});
};


