const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

//Token Creation
module.exports.createAccessToken = (user) => {
	//this is the payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//jwt.sign(data/payload, secretkey, options)
	return jwt.sign(data, secret, {});
}




//Token Verification
//WHAT IS THE USE OF THE NEXT PARAMETER??
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
		//console.log(token);
		// console.log(typeof token)

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			}else{
				console.log(typeof next);
				console.log(next)
				next();
			}
		})

	}else {
		return res.send({auth: "failed"})
	}

}

//Token Decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);


		return jwt.verify(token, secret, (error,data) => {

			if(error){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null
	}
}








